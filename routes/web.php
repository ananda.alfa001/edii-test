<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\auth\LoginController;
use App\Http\Controllers\auth\RegisterController;
use App\Http\Controllers\auth\GuardController;
use App\Http\Controllers\auth\ActorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::middleware(['guest'])->group(function () {
    Route::post('/login',[LoginController::class,'authenticate'])->name('login');
    Route::get('/register',[RegisterController::class,'register'])->name('register');
    Route::post('/register-up',[RegisterController::class,'store'])->name('register-up');
});

Route::middleware(['auth'])->group(function () {
    //SESSION GUARD
    Route::get('/Guard',[GuardController::class,'AuthGuard'])->name('AuthGuard');
    Route::get('/Auth-Roles',[GuardController::class,'AuthRoles'])->name('AuthRoles');
    Route::get('/dashboard-admin',[ActorController::class,'show']);
    Route::get('/dashboard-user',[ActorController::class,'show']);
    Route::post('/entry',[ActorController::class,'entry'])->name('entry');
    Route::get('/logout',[GuardController::class,'logout'])->name('logout');
});
