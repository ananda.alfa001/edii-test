<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="EDII">
    <meta name="theme-color" content="#75dab4" />
    <title>Admin | PT. EDII</title>
    <meta name="EDII" content="EDII">
    <meta name="description" content="Admin  | PT. EDII">
    @include('main.components.style')
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    @include('sweetalert::alert')
    <div class="wrapper">
        @include('main.components.navbar')
        @include('main.components.sidebar')
        {{-- ============= --}}
        <div class="content-wrapper">
            @yield('content')
        </div>
        {{-- ============= --}}
        @include('main.components.footer')
        @include('main.components.script')
    </div>
</body>

</html>
