@extends('panel-admin-rttmc.main.main')
@section('content')
    <section class="content-header">


        <section class="content">
            <div class="container-fluid">
                <div class="card-body">
                    <input type="hidden" name="has_survey" id="has_survey" value="1">
                    <div class="row_survey">
                    </div>
                    <div class="row_presensi">
                        <div class="row div-presensi">
                            <div class="col-md-6">
                                <div class="well ">
                                    <h3 class="h3 text-info">Presensi Pegawai RTTMC</h3>
                                    <p><small>Status Presensi Anda Hari Ini</small></p>
                                    <hr>
                                    <div align="center" class="form-control-static" style="text-transform: uppercase;">
                                        <strong>
                                            {{ Auth::user()->name }}
                                            <br />
                                            NIP: {{ Auth::user()->NIP }}<br />
                                            {{ Auth::user()->posisi }}</strong>
                                    </div>
                                    <hr>
                                    
                                    {{-- Absensi Selesai1 --}}
                                    <div class="info-well success">
                                        <div class="info-well-text">
                                            Anda telah tercatat melakukan <strong><span class="badge bg-success">Sudah Melakukan Presensi</span></strong> dan
                                            <strong>Selesai Bekerja</strong> di Sistem Absensi RTTMC, dengan durasi kerja <b>10 Jam 51 Menit</b>.


                                            Anda tercatat <strong><span class="badge bg-danger">Belum Melakukan Presensi.</span></strong> 
                                            <strong>Silahkan lakukan presensi terlebih dahulu!</strong>

                                            <br><br>
                                            <div class="well well-sm">
                                                <ol class="text-bold" style="padding-left: 20px; margin: 0px">Work
                                                    From Office (WFO) </ol>
                                                <ul style="padding-left: 20px; margin: 0px">
                                                    <li>Jam Presensi: <b>07:00:00</b> s/d <b>21:00:00</b></li>
                                                    <li>Jam Kerja: <b>08:00:00</b> s/d <b>20:00:00</b> (Shift 1 Pagi)</li>
                                                    <li>Jam Kerja: <b>20:00:00</b> s/d <b>08:00:00</b> (Shift 2 Malam)</li>
                                                </ul>
                                            </div>
                                            <div class="well well-sm">
                                                <ol class="text-bold" style="padding-left: 20px; margin: 0px">Work
                                                    From Home (WFH)</ol>
                                                <ul style="padding-left: 20px; margin: 0px">
                                                    <li>Jam Presensi: <b>07:00:00</b> s/d <b>21:00:00</b></li>
                                                    <li>Jam Kerja: <b>08:00:00</b> s/d <b>20:00:00</b> (Shift 1 Pagi)</li>
                                                    <li>Jam Kerja: <b>20:00:00</b> s/d <b>08:00:00</b> (Shift 2 Malam)</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <p>Status Presensi Anda Hari Ini</p> -->
                                    <div class="block-info-presensi">
                                        <table class="table table-condensed block-info-presensi">
                                            <tbody>

                                                <tr class="block-info-presensi">
                                                    <td><label class="control-label">Mulai Bekerja</label></td>
                                                    <td>
                                                        <div class="form-control-static waktu_presensi pull-right">
                                                            2022-04-01 07:02:23</div>
                                                    </td>
                                                </tr>
                                                <tr class="block-info-presensi">
                                                    <td><label class="control-label">Selesai Bekerja</label></td>
                                                    <td>
                                                        <div class="form-control-static waktu_presensi pull-right">
                                                            2022-04-01 17:52:25</div>
                                                    </td>
                                                </tr>
                                                <tr class="block-info-presensi">
                                                    <td><label class="control-label">Lokasi</label></td>
                                                    <td>
                                                        <div class="form-control-static lokasi pull-right">Gedung Kementerian Perhubungan Republik Indonesia - Jl. Medan Merdeka Barat No.8, RT.2/RW.3, Gambir, Kecamatan Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10110</div>
                                                    </td>
                                                </tr>
                                                <tr class="block-info-presensi">
                                                    <td colspan="2" align="center">
                                                        <p><span class="label label-success"><i class="fa fa-check"></i>
                                                                Work From Office</span>
                                                            <span class="label label-default"><i class="fa fa-ban"></i>
                                                                Work From Home</span>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    {{-- ---------------- --}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="well ">
                                    <h3 class="h3 text-info">Profil
                                        <div class="btn-group pull-right" style="margin-left: 20px;">
                                            <a class="btn btn-success btn-sm btn-form-profil"
                                                href="https://apps.ub.ac.id/kepegawaian/administrasi/datadiri"><i
                                                    class="fa fa-edit"></i></a>
                                        </div>
                                    </h3>
                                    <hr>
                                    <form id="frm-photo" class="form-core-profile form-horizontal frm-photo"
                                        action="https://apps.ub.ac.id/kepegawaian/administrasi/savepegawaiphoto"
                                        method="post" autocomplete="off" onsubmit="return false;">
                                        <div align="center">
                                            <figure style="max-width: 152px;">
                                                <img id="img_photo" name="img_photo"
                                                    src="https://apps.ub.ac.id/assets/images/avatars/11.png" alt=""
                                                    class="img-rounded img-responsive">
                                            </figure>

                                        </div>
                                    </form>
                                    <table class="table table-condensed">
                                        <tr class="block-nidn">
                                            <td><label class="control-label">Nama Lengkap Pegawai</label></td>
                                            <td> <span class="form-control-static">{{ Auth::user()->name }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><label class="control-label">NIP</label></td>
                                            <td> <span class="form-control-static ">{{ Auth::user()->NIP }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><label class="control-label">E-Mail Pegawai</label></td>
                                            <td> <span class="form-control-static ">{{ Auth::user()->email }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><label class="control-label">Posisi Pegawai</label></td>
                                            <td> <span class="form-control-static">{{ Auth::user()->posisi }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><label class="control-label">Tanggal Lahir</label></td>
                                            <td> <span class="form-control-static">{{ Auth::user()->birthdate }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><label class="control-label">Jenis Kelamin</label></td>
                                            <td> <span class="form-control-static"></span> {{ Auth::user()->jenis_kelamin }}</td>
                                        </tr>
                                        <tr>
                                            <td><label class="control-label">Alamat</label></td>
                                            <td> <span class="form-control-static ">{{ Auth::user()->alamat }}</span></td>
                                        </tr>
                                        <tr>
                                            <td><label class="control-label">Deskripsi Pegawai</label></td>
                                            <td> <span class="form-control-static ">{{ Auth::user()->deskripsi }}</span></td>
                                        </tr>
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="well ">
                                <h3 class="h3 text-info">Tugas Yang Dilaksanakan</h3>
                                <p><small>Tugas dapat dimasukkan ketika Anda sudah melakukan Rekam
                                        Presensi.</small></p>
                                <hr>
                                <div class="card card-primary">
                                    <div class="card-header">
                                      <h3 class="card-title">Road Transport and Traffic Management Center - Shift 1</h3>
                                    </div>
                                    <form method="POST" action="{{route('in')}}">
                                        @csrf
                                      <div class="card-body">
                                        <div class="form-group">
                                        <textarea name="pekerjaan" class="form-control" rows="3" placeholder="Tuliskan Pekerjaan yang anda lakukan..."></textarea>
                                        </div>
                                        <div class="form-group">
                                            <textarea name="pekerjaan" class="form-control" rows="3" placeholder="Tuliskan Pekerjaan yang anda lakukan..."></textarea>
                                        </div>
                                      <div class="card-footer">
                                        <button type="submit" class="btn btn-primary">Akhiri Kerja Sekarang</button>
                                      </div>
                                    </form>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection
