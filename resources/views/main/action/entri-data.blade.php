@extends('main.main')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Entri Data</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Entri Data</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-info">
                <div class="card-header">
                  <h3 class="card-title">DATA PRIBADI PELAMAR</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal">
                  <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">POSISI YANG DILAMAR</label>
                      <div class="col-sm-10">
                        <input type="text" name="posisi" class="form-control" placeholder="Posisi yang anda lamar ...">
                      </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">NAMA</label>
                        <div class="col-sm-10">
                          <input type="text" name="nama" class="form-control" placeholder="Nama Anda ...">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">NOMOR KTP</label>
                        <div class="col-sm-10">
                          <input type="number" name="ktp" class="form-control" placeholder="Nomor KTP Anda ...">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">TEMPAT, TANGGAL LAHIR</label>
                        <div class="col-sm-10">
                          <input type="text" name="ktp" class="form-control" placeholder="Tempat tanggal lahir Anda ...">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">JENIS KELAMIN</label>
                        <div class="col-sm-10">
                          <input type="text" name="jenis_kelamin" class="form-control" placeholder="Jenis Kelamin Anda ...">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">AGAMA</label>
                        <div class="col-sm-10">
                          <input type="text" name="agama" class="form-control" placeholder="agama Anda ...">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">GOLONGAN DARAH</label>
                        <div class="col-sm-10">
                          <input type="text" name="agama" class="form-control" placeholder="agama Anda ...">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">STATUS</label>
                        <div class="col-sm-10">
                          <input type="text" name="status" class="form-control" placeholder="Status Anda ...">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">ALAMAT KTP</label>
                        <div class="col-sm-10">
                          <textarea type="text" name="alamat_ktp" class="form-control" placeholder="Alamat Sesuai KTP Anda ..."></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">ALAMAT TEMPAT TINGGAL</label>
                        <div class="col-sm-10">
                          <textarea type="text" name="alamat_tinggal" class="form-control" placeholder="Alamat Tempat Tinggal Anda ..."></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">EMAIL</label>
                        <div class="col-sm-10">
                          <input type="email" name="email" class="form-control" placeholder="Email Anda ..."></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">NO. TELEPON</label>
                        <div class="col-sm-10">
                          <input type="number" name="no_telp" class="form-control" placeholder="Nomor Telepon Anda ..."></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">NOMOR ORANG TERDEKAT YANG DAPAT DIHUBUNGI</label>
                        <div class="col-sm-10">
                          <input type="number" name="no_telp_dekat" class="form-control" placeholder="Nomor Kerabat Dekat Anda ..."></textarea>
                        </div>
                    </div>
                    <hr>
                    <label><h4>PENDIDIKAN TERAKHIR</h4></label>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                          <!-- select -->
                          <div class="form-group">
                            <label>JENJANG PENDIDIKAN TERAKHIR</label>
                            <input type="text" name="jenjang" class="form-control" >
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label>NAMA INSTITUSI AKADEMIK</label>
                            <input type="text" name="institusi" class="form-control" >
                          </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                              <label>JURUSAN</label>
                              <input type="text" name="jurusan" class="form-control" >
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="form-group">
                              <label>TAHUN LULUS</label>
                              <input type="text" name="tahun_lulus" class="form-control" >
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="form-group">
                              <label>IPK</label>
                              <input type="number" name="ipk" class="form-control">
                            </div>
                          </div>
                    </div>
                    <hr>
                    <label><h4>RIWAYAT PELATIHAN</h4></label>
                    <hr>
                    <div class="row">
                        <div class="col-sm-4">
                          <!-- select -->
                          <div class="form-group">
                            <label>NAMA KHURSUS/SEMINAR</label>
                            <input type="khursus" name="jenjang" class="form-control" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>SERTIFIKAT (ADA/TIDAK)</label>
                            <input type="sertifikat" name="institusi" class="form-control" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                              <label>TAHUN</label>
                              <input type="tahun_sertifikat" name="jurusan" class="form-control" >
                            </div>
                          </div>
                      </div>
                  </div>

                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-info">Entry Data</button>
                    <button type="submit" class="btn btn-default float-right">Cancel</button>
                  </div>
                  <!-- /.card-footer -->
                </form>
              </div>
        </div>
    </section>
@endsection
