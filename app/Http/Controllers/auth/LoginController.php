<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Alert;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function authenticate(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validate->fails()) {
            Alert::info('Info', $validate->errors()->first());
            return redirect()->back();
        }else{
            $credentials = $request->validate([
                'email' => 'required',
                'password' => 'required',
            ]);
            $remember = $request->input('remember');
            if (Auth::attempt($credentials, $remember)) {
                $request->session()->regenerate();
                $name = Auth::user()->name;
                Alert::success('Login Success', 'Have a Nice Work '.$name.'!');
                return redirect()->intended('/Auth-Roles');
            }else{
                Alert::warning('Username atau Password Salah!', 'Coba Lagi!');
                return back();
            }
        }
    }
   
}
