<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Alert;

class GuardController extends Controller
{
    //first login
    public function AuthRoles(request $request)
    {   
        if(Auth::check()){
                if(Auth::user()->roles == "admin" && session('alert')){
                        $name = Auth::user()->name;
                        Alert::success('Login Success', 'Have a Nice Work '.$name.'!');
                        return redirect('/dashboard-admin');
                    }elseif(Auth::user()->roles == "admin"){
                        return redirect('/dashboard-admin');
                    }elseif(Auth::user()->roles == "user" && session('alert')){
                        return redirect('/dashboard-user');
                    }elseif(Auth::user()->roles == "user"){
                        return redirect('/dashboard-user');
                    }else{
                        Auth::logout();
                        request()->session()->invalidate();
                        request()->session()->regenerateToken();
                        Alert::error('Login Gagal', 'Coba Kembali!');
                        return redirect('/');
                    }
        }else{
            Auth::logout();
            request()->session()->invalidate();
            request()->session()->regenerateToken();
            Alert::error('Login Gagal', 'Coba Kembali!');
            return redirect('/');
        }
    }

    public function AuthGuard(request $request)
    {  
        if(Auth::check()){
            if(Auth::user()->roles == "admin" && session('alert')){
                    $name = Auth::user()->name;
                    Alert::success('Login Success', 'Have a Nice Work '.$name.'!');
                    return redirect('/dashboard-admin');
                }elseif(Auth::user()->roles == "admin"){
                    return redirect('/dashboard-admin');
                }elseif(Auth::user()->roles == "user" && session('alert')){
                    return redirect('/dashboard-user');
                }elseif(Auth::user()->roles == "user"){
                    return redirect('/dashboard-user');
                }else{
                    Auth::logout();
                    request()->session()->invalidate();
                    request()->session()->regenerateToken();
                    Alert::error('Login Gagal', 'Coba Kembali!');
                    return redirect('/');
                }
        }else{
            Alert::error('Anda belum Log In', 'silahkan Log In terlebih dahulu!');
            return redirect('/');
        }
    }
    public function logout(Request $request){
        Auth::logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        Alert::success('Logout Success', 'Good Bye!');
        return redirect('/');    
    }
}
