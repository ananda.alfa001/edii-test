<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use App\Models\User;
use Carbon\Carbon;
use Alert;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    protected $redirectTo = RouteServiceProvider::HOME;
    
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function register()
    {
        return view('register');
    }

    protected function store(request $request)
    {
        
        $validate = Validator::make($request->all(), [
            'email' => 'required', 'string', 'max:255', 'unique:users',
            'password' => 'required',
        ]);
        if ($validate->fails()) {
            Alert::info('Info', $validate->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $validate = $request->validate([
                        'email' => ['required'],
                        'password' => ['required'],
                        'roles' => [],
                    ]);
                    $validate['password'] = Hash::make($validate['password']);
                    $validate['roles'] = "user"; 
                    $user = User::create($validate);
                    Auth::login($user);
                    Alert::Success('Success',' Berhasil Terdaftar dan Log In!');
                    return redirect('/dashboard-user');
                }catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
        }
    }
    protected function store_admin(request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required', 'string', 'max:255', 'unique:users',
            'password' => 'required',
        ]);
        if ($validate->fails()) {
            Alert::info('Info', $validate->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $validate = $request->validate([
                        'email' => ['required'],
                        'password' => ['required'],
                        'roles' => [],
                    ]);
                    $validate['password'] = Hash::make($validate['password']);
                    $validate['roles'] = "admin"; 
                    $user = User::create($validate);
                    Auth::login($user);
                    Alert::Success('Success',' Berhasil Terdaftar dan Log In!');
                    return redirect('/dashboard-admin');
                }catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
        }
    }
}
   
